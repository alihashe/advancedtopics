﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerBox : MonoBehaviour
{

    public static float speed = 6.0f;
    public ItemData itemData;

    private Vector3 moveDirection = Vector3.zero;

    void Update()
    {
            moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0.0f, Input.GetAxis("Vertical"));
            moveDirection *= speed;

        transform.position += moveDirection * Time.deltaTime;
    }

    private void OnTriggerEnter(Collider other)
    {
        foreach (Item item in itemData.items)
        {
            Debug.Log("end");
            print(item.title);
        }
    }
}