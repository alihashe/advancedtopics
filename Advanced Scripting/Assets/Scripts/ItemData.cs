﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ItemData : MonoBehaviour
{
    public List<Item> items = new List<Item>();

    private void Awake()
    {
        BuildDatabase();
    }

    public Item GetItem(int id)
    {
        return items.Find(item => item.id == id);
    }

    public Item GetItem(string itemName)
    {
        return items.Find(item => item.title == itemName);
    }
    void BuildDatabase()
    {
        List<Item> Item = new List<Item>()
        {
            new Item(0, "Big Sword", "A very, very big sword",
            new Dictionary<string, int>
            {
                {"Power", 15 },
                {"Defence", 10 }
            }),
            new Item(1, "Small Shield", "A very, very small shield",
            new Dictionary<string, int>
            {
                {"Value", 100 }
            }),
            new Item(2, "Big Gold", "A very valuable piece of gold",
            new Dictionary<string, int>
            {
                {"Power", 5 },
                {"Value", 444 }
            })
    };
    }
}