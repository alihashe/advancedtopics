﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;


    public class Item : IComparable<Item>
{
        public int id;
        public string title;
        public string description;
        public Dictionary<string, int> stats = new Dictionary<string, int>();

        public Item(int id, string title, string description, 
            Dictionary<string, int> stats)

        {
            this.id = id;
            this.title = title;
            this.description = description;
            this.stats = stats;
        }
        public Item(Item item)
        {
            this.id = item.id;
            this.title = item.title;
            this.description = item.description;
            this.stats = item.stats;
        }
        public int CompareTo(Item other)
        {
        if (other == null)
            {
                return 1;
            }

            return id - other.id;
        }
}